## Start Server

Location: src/main/java/com/example/marketmaker/program/

1. It starts with default port 8081.
2. It spawns a new thread each time a new clinet application connects. 
3. It calculates quotes concurrently for a group of order messages from a client and returns quotes. 

## Test Cases

Location: src/test/java

1. It has basic unit test cases for classes used.


-----

## Stock Security Id 

1. It has to be of 3 digits ranging from 100 to 299.
2. Id starting with 1 means liquid category and illiquid category (where Liquidity Premium applies to Quotes) otherwise.


## Spread Calculation 

1. Basic Spread: 2bp
2. Block Deal Spread: 1.5bp
3. Liquidity Premium: 1bp
4. Block deal is defined as order size with more than 10,000 shares.


-----

## Logs

2021-07-02 08:32:56.238|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|The stock market maker server is running...
2021-07-02 08:33:03.972|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Connected: Socket[addr=/127.0.0.1,port=3603,localport=8081]
2021-07-02 08:33:03.98|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 120 BUY 153
2021-07-02 08:33:03.98|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 121 BUY 306
2021-07-02 08:33:03.98|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 122 BUY 459
2021-07-02 08:33:03.98|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 123 BUY 612
2021-07-02 08:33:03.98|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 124 BUY 765
2021-07-02 08:33:03.98|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 125 BUY 918
2021-07-02 08:33:03.981|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 126 BUY 1071
2021-07-02 08:33:03.981|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 127 BUY 1224
2021-07-02 08:33:03.981|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 128 BUY 1377
2021-07-02 08:33:03.981|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 129 BUY 1530
2021-07-02 08:33:03.981|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 220 SELL 495
2021-07-02 08:33:03.981|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 221 SELL 540
2021-07-02 08:33:03.982|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 222 SELL 585
2021-07-02 08:33:03.983|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 223 SELL 630
2021-07-02 08:33:03.983|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 224 SELL 675
2021-07-02 08:33:03.983|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 225 SELL 720
2021-07-02 08:33:03.983|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 226 SELL 765
2021-07-02 08:33:03.984|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 227 SELL 810
2021-07-02 08:33:03.984|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 228 SELL 855
2021-07-02 08:33:03.986|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Order Received: 229 SELL 900
2021-07-02 08:33:07.004|com.example.marketmaker.StockOrderProcessor|Calculation is done : 120 BUY 153 10.232046
2021-07-02 08:33:07.004|com.example.marketmaker.StockOrderProcessor|Calculation is done : 122 BUY 459 5.1130224
2021-07-02 08:33:07.004|com.example.marketmaker.StockOrderProcessor|Calculation is done : 121 BUY 306 4.640928
2021-07-02 08:33:07.004|com.example.marketmaker.StockOrderProcessor|Calculation is done : 123 BUY 612 75.75834864000001
2021-07-02 08:33:10.005|com.example.marketmaker.StockOrderProcessor|Calculation is done : 125 BUY 918 12.232446000000001
2021-07-02 08:33:10.019|com.example.marketmaker.StockOrderProcessor|Calculation is done : 124 BUY 765 23.2606512
2021-07-02 08:33:10.019|com.example.marketmaker.StockOrderProcessor|Calculation is done : 127 BUY 1224 56.1232224
2021-07-02 08:33:10.019|com.example.marketmaker.StockOrderProcessor|Calculation is done : 126 BUY 1071 56.61132
2021-07-02 08:33:13.013|com.example.marketmaker.StockOrderProcessor|Calculation is done : 128 BUY 1377 7527.2483806464
2021-07-02 08:33:13.029|com.example.marketmaker.StockOrderProcessor|Calculation is done : 221 SELL 540 45.736275
2021-07-02 08:33:13.029|com.example.marketmaker.StockOrderProcessor|Calculation is done : 220 SELL 495 5.69829
2021-07-02 08:33:13.03|com.example.marketmaker.StockOrderProcessor|Calculation is done : 129 BUY 1530 213.275206512
2021-07-02 08:33:16.019|com.example.marketmaker.StockOrderProcessor|Calculation is done : 222 SELL 585 754.443599
2021-07-02 08:33:16.034|com.example.marketmaker.StockOrderProcessor|Calculation is done : 224 SELL 675 74.10076310000001
2021-07-02 08:33:16.034|com.example.marketmaker.StockOrderProcessor|Calculation is done : 223 SELL 630 665.680236
2021-07-02 08:33:16.035|com.example.marketmaker.StockOrderProcessor|Calculation is done : 225 SELL 720 55.68329
2021-07-02 08:33:19.03|com.example.marketmaker.StockOrderProcessor|Calculation is done : 226 SELL 765 74.617608
2021-07-02 08:33:19.045|com.example.marketmaker.StockOrderProcessor|Calculation is done : 228 SELL 855 74.10076310000001
2021-07-02 08:33:19.045|com.example.marketmaker.StockOrderProcessor|Calculation is done : 227 SELL 810 615.695236
2021-07-02 08:33:19.045|com.example.marketmaker.StockOrderProcessor|Calculation is done : 229 SELL 900 4.1187640000000005
2021-07-02 08:33:19.047|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 120 BUY 153: 10.232046
2021-07-02 08:33:19.049|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 121 BUY 306: 4.640928
2021-07-02 08:33:19.056|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 122 BUY 459: 5.1130224
2021-07-02 08:33:19.057|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 123 BUY 612: 75.75834864000001
2021-07-02 08:33:19.058|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 124 BUY 765: 23.2606512
2021-07-02 08:33:19.059|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 125 BUY 918: 12.232446000000001
2021-07-02 08:33:19.06|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 126 BUY 1071: 56.61132
2021-07-02 08:33:19.061|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 127 BUY 1224: 56.1232224
2021-07-02 08:33:19.062|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 128 BUY 1377: 7527.2483806464
2021-07-02 08:33:19.063|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 129 BUY 1530: 213.275206512
2021-07-02 08:33:19.063|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 220 SELL 495: 5.69829
2021-07-02 08:33:19.064|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 221 SELL 540: 45.736275
2021-07-02 08:33:19.065|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 222 SELL 585: 754.443599
2021-07-02 08:33:19.066|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 223 SELL 630: 665.680236
2021-07-02 08:33:19.067|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 224 SELL 675: 74.10076310000001
2021-07-02 08:33:19.068|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 225 SELL 720: 55.68329
2021-07-02 08:33:19.068|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 226 SELL 765: 74.617608
2021-07-02 08:33:19.069|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 227 SELL 810: 615.695236
2021-07-02 08:33:19.07|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 228 SELL 855: 74.10076310000001
2021-07-02 08:33:19.071|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Quote Sent for 229 SELL 900: 4.1187640000000005
2021-07-02 08:33:19.078|com.example.marketmaker.program.StockMarketMakerServerMultiThreaded|Closed: Socket[addr=/127.0.0.1,port=3603,localport=8081]