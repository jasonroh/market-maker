package com.example.marketmaker;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;

public class BloombergPriceSource implements ReferencePriceSource {
	private HashMap<Integer, Double> bloombergPrices = new HashMap<Integer, Double>();
	private List<ReferencePriceSourceListener> listeners = new ArrayList<ReferencePriceSourceListener>();

	public BloombergPriceSource() {
		bloombergPrices.put(120, 10.23);
		bloombergPrices.put(121, 4.64);
		bloombergPrices.put(122, 5.112);
		bloombergPrices.put(123, 75.7432);
		bloombergPrices.put(124, 23.256);
		bloombergPrices.put(125, 12.23);
		bloombergPrices.put(126, 56.6);
		bloombergPrices.put(127, 56.112);
		bloombergPrices.put(128, 7525.743232);
		bloombergPrices.put(129, 213.23256);

		bloombergPrices.put(220, 5.7);
		bloombergPrices.put(221, 45.75);
		bloombergPrices.put(222, 754.67);
		bloombergPrices.put(223, 665.88);
		bloombergPrices.put(224, 74.123);
		bloombergPrices.put(225, 55.7);
		bloombergPrices.put(226, 74.64);
		bloombergPrices.put(227, 615.88);
		bloombergPrices.put(228, 74.123);
		bloombergPrices.put(229, 4.12);

		// ....
	}

	public double get(int securityId) {
		return bloombergPrices.getOrDefault(securityId, 100.0);
	}

	public void referencePriceChanged(int securityId, double price) {
		bloombergPrices.put(securityId, price);

		for (ReferencePriceSourceListener listener : listeners) {
			listener.referencePriceChanged(securityId, price);
		}
	}

	public void subscribe(ReferencePriceSourceListener listener) {
		listeners.add(listener);
	}
}