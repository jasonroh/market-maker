package com.example.marketmaker;

public class BloombergPriceSourceListener implements ReferencePriceSourceListener {
	private String name;

	public BloombergPriceSourceListener(String name) {
		this.name = name;
	}

	public void referencePriceChanged(int securityId, double price) {
		System.out.printf("%s - Stock Price Change: %d: %.4f\n", this.name, securityId, price);
	}
}
