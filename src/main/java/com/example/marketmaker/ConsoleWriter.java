package com.example.marketmaker;

import java.sql.Timestamp;

public class ConsoleWriter {
	public static void WriteToConsole(String message, String className) {
		System.out.println(new Timestamp(new java.util.Date().getTime()) + "|" + className + "|"+ message);
	}
}
