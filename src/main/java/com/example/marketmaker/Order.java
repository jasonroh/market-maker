package com.example.marketmaker;

public class Order {
	private int securityId;
	private int quantity;
	private Side side;

	private String[] validate(String orderMessage) throws Exception {
		String[] parts = orderMessage.split(" ");
		if (parts.length != 3)
			throw new Exception("Order Message is not valid");
		return parts;
	}

	public Order(String orderMessage) throws Exception {
		try {
			String[] parts = this.validate(orderMessage);
			this.securityId = Integer.parseInt(parts[0]);
			this.side = Side.valueOf(parts[1]);
			this.quantity = Integer.parseInt(parts[2]);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		}
	}

	public int getSecurityId() {
		return this.securityId;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public Side getSide() {
		return this.side;
	}
}