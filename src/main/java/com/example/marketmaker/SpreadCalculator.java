package com.example.marketmaker;

public interface SpreadCalculator {
	double calculateSpread(int securityId, double referencePrice, int quantity);
}
