package com.example.marketmaker;

import java.util.HashMap;

public class StockOrderProcessor {
	public static double calculateQuote(String orderMessage, ReferencePriceSource priceSource,
			QuoteCalculationEngine engine) throws Exception {
		Order order = new Order(orderMessage);
		if (!StockSecurityIdValidator.validateSecurityId(order.getSecurityId()))
			throw new Exception("Invalid Security Id");
		boolean isBuy = order.getSide() == Side.BUY;
		double quote = engine.calculateQuotePrice(order.getSecurityId(), priceSource.get(order.getSecurityId()), isBuy,
				order.getQuantity());
		ConsoleWriter.WriteToConsole("Calculation is done : " + orderMessage + " " + Double.toString(quote),
				StockOrderProcessor.class.getName());
		return quote;
	}

	public static HashMap<String, Double> calculateQuotes(String[] orderMessages, ReferencePriceSource priceSource,
			QuoteCalculationEngine engine) throws Exception {
		HashMap<String, Double> quotes = new HashMap<String, Double>(orderMessages.length);

		for (String orderMessage : orderMessages) {
			double quote = 0.0;

			try {
				quote = StockOrderProcessor.calculateQuote(orderMessage, priceSource, engine);
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				quotes.put(orderMessage, quote);
			}
		}

		return quotes;
	}
}
