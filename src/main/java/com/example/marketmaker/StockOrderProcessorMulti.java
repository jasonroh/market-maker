package com.example.marketmaker;

import java.util.concurrent.*;

public class StockOrderProcessorMulti extends StockOrderProcessor {
	public ConcurrentHashMap<String, Double> calculateQuotesConccurent(String[] orderMessages,
			ReferencePriceSource priceSource, QuoteCalculationEngine engine) {
		ConcurrentHashMap<String, Double> quoteMap = new ConcurrentHashMap<>(orderMessages.length);
		ExecutorService executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

		for (String orderMessage : orderMessages) {
			executor.execute(new QuoteEngineRunner(quoteMap, orderMessage, engine, priceSource));
		}
		executor.shutdown();

		try {
			executor.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}

		return quoteMap;
	}

	class QuoteEngineRunner implements Runnable {
		private ConcurrentHashMap<String, Double> quoteMap;
		private String orderMessage;
		private QuoteCalculationEngine engine;
		private ReferencePriceSource priceSource;

		QuoteEngineRunner(ConcurrentHashMap<String, Double> quoteMap, String orderMessage,
				QuoteCalculationEngine engine, ReferencePriceSource priceSource) {
			this.quoteMap = quoteMap;
			this.orderMessage = orderMessage;
			this.engine = engine;
			this.priceSource = priceSource;
		}

		@Override
		public void run() {
			double quote = 0.0;

			try {
				quote = StockOrderProcessorMulti.calculateQuote(orderMessage, priceSource, engine);
			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				this.quoteMap.put(orderMessage, quote);
			}
		}
	}
}