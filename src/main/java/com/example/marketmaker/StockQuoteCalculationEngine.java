package com.example.marketmaker;

public class StockQuoteCalculationEngine implements QuoteCalculationEngine {
	private SpreadCalculator spreadCalculator;

	public StockQuoteCalculationEngine() {
		this.spreadCalculator = new StockSpreadCalculator();
	}

	public double calculateQuotePrice(int securityId, double referencePrice, boolean buy, int quantity) {
		double spread = this.spreadCalculator.calculateSpread(securityId, referencePrice, quantity);
		double multiplyFactor = buy ? 1 : -1;
		spread = spread * multiplyFactor;

		// Mock the situation that it takes 3 seconds to calculate quote price
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return referencePrice + spread;
	}
}