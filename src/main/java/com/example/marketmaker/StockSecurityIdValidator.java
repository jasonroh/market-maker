package com.example.marketmaker;

public class StockSecurityIdValidator {
	public static boolean validateSecurityId(int securityId) {
		/**
		 * stock security id should be of 3 digits stock security id should start with
		 * either 1 or 2
		 */
		String securityCode = Integer.toString(securityId);

		if (!securityCode.startsWith("1") && !securityCode.startsWith("2"))
			return false;
		if (securityCode.length() != 3)
			return false;

		return true;
	}

	public static boolean validateSecurityId(String securityId) {
		/**
		 * stock security id should be of 3 digits stock security id should start with
		 * either 1 or 2
		 */
		if (!securityId.startsWith("1") && !securityId.startsWith("2"))
			return false;
		if (securityId.length() != 3)
			return false;

		return true;
	}
}
