package com.example.marketmaker;

public class StockSpreadCalculator implements SpreadCalculator {
	private final int BLOCK_DEAL_QUANTITY = 10000;
	private final double LIQUITY_PREMIUM = 0.01;

	private final double STOCK_SPREAD = 0.02;
	private final double STOCK_BLOCK_SPREAD = 0.015;

	public StockSpreadCalculator() {
	}

	public double calculateSpread(int securityId, double referencePrice, int quantity) {
		double spread = this.findSpread(securityId, quantity);
		return spread * referencePrice * 0.01;
	}

	private double findSpread(int securityId, int quantity) {
		/***
		 * Find spread of security based on liquidity and quantity of order
		 * 
		 * @param securityId security identifier
		 * @param quantity   order quantity
		 */

		if (this.isLiquidStock(securityId)) {
			if (quantity < this.BLOCK_DEAL_QUANTITY)
				return this.STOCK_SPREAD;
			return this.STOCK_BLOCK_SPREAD;
		}

		if (quantity < this.BLOCK_DEAL_QUANTITY)
			return this.STOCK_SPREAD + this.LIQUITY_PREMIUM;
		return this.STOCK_BLOCK_SPREAD + this.LIQUITY_PREMIUM;
	}

	private boolean isLiquidStock(int securityId) {
		/***
		 * Stock ID starting with 1 : Liquid Stock Stock ID starting with 2 : Not Liquid
		 * Stock - Liquidity Premium Applies
		 * 
		 * @param securityId security identifier
		 */

		return Integer.toString(securityId).startsWith("1");
	}
}