package com.example.marketmaker.program;

import java.net.*;
import java.io.*;

public class StockMarketMakerClient {
	public static final String HOSTNAME = "localhost";
	public static final int PORT = 8081;
	public static final int ORDER_SIZE = 20;

	public static void main(String[] args) {
		String[] messages = StockMarketMakerClient.createOrderMessages();

		try (Socket socket = new Socket(HOSTNAME, PORT)) {
			for (int i = 0; i < ORDER_SIZE; i++) {
				PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
				out.println(messages[i]);
			}
		} catch (UnknownHostException ex) {
			System.out.println("Server not found: " + ex.getMessage());
		} catch (IOException ex) {
			System.out.println("I/O error: " + ex.getMessage());
		}
	}

	private static String[] createOrderMessages() {
		String[] messages = new String[ORDER_SIZE];

		for (int i = 0; i < 10; i++) {
			messages[i] = Integer.toString(i + 120) + " BUY " + Integer.toString((i + 1) * 153);
		}
		for (int i = 10; i < ORDER_SIZE; i++) {
			messages[i] = Integer.toString(i + 210) + " SELL " + Integer.toString((i + 1) * 45);
		}
		return messages;
	}
}