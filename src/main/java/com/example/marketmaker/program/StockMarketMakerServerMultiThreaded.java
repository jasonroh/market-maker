package com.example.marketmaker.program;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.concurrent.Executors;

import java.util.*;

import com.example.marketmaker.BloombergPriceSource;
import com.example.marketmaker.ConsoleWriter;
import com.example.marketmaker.QuoteCalculationEngine;
import com.example.marketmaker.ReferencePriceSource;
import com.example.marketmaker.StockOrderProcessorMulti;
import com.example.marketmaker.StockQuoteCalculationEngine;

public class StockMarketMakerServerMultiThreaded {
	public static final int port = 8081;
	
    public static void main(String[] args) throws Exception {
        try (var listener = new ServerSocket(port)) {
        	ConsoleWriter.WriteToConsole("The stock market maker server is running...", StockMarketMakerServerMultiThreaded.class.getName());
            var pool = Executors.newFixedThreadPool(20);
            while (true) {
                pool.execute(new MarketMaker(listener.accept()));
            }
        }
    }
    
    private static class MarketMaker implements Runnable {
        private Socket socket;
        private StockOrderProcessorMulti processor = new StockOrderProcessorMulti();
    	private QuoteCalculationEngine quoteCalculationEngine = new StockQuoteCalculationEngine();
    	private ReferencePriceSource referencePriceSource = new BloombergPriceSource();
        
        MarketMaker(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
        	ConsoleWriter.WriteToConsole("Connected: " + socket, StockMarketMakerServerMultiThreaded.class.getName());
            try {
                var in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                
                String orderMessage;
                var orderMessages = new ArrayList<String>();
                
                while((orderMessage=in.readLine())!=null){
                    ConsoleWriter.WriteToConsole("Order Received: " + orderMessage, StockMarketMakerServerMultiThreaded.class.getName());
                    orderMessages.add(orderMessage);
                }
                
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                
                var quotes = processor.calculateQuotesConccurent(orderMessages.toArray(new String[0]), 
                		referencePriceSource, quoteCalculationEngine);
                
                for(String orderMsg : orderMessages) {
                	ConsoleWriter.WriteToConsole("Quote Sent for " + orderMsg + ": " + Double.toString(quotes.get(orderMsg)), StockMarketMakerServerMultiThreaded.class.getName());
                	out.println(quotes.get(orderMsg));
                }
            } catch (Exception ex) {
            	ex.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                	e.printStackTrace();
                }
                ConsoleWriter.WriteToConsole("Closed: " + socket, StockMarketMakerServerMultiThreaded.class.getName());
            }
		}
	}
}
