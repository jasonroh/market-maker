package com.example.marketmaker.testcase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.example.marketmaker.*;

public class BloombergPriceSourceTest {
	private BloombergPriceSource pricingSource;

	@BeforeEach
	void setUp() throws Exception {
		pricingSource = new BloombergPriceSource();
	}

	@Test
	@DisplayName("Price of security 120 should be 10.23")
	public void testPriceOfSecurity120() {
		assertEquals(10.23, pricingSource.get(120), "Price of security 120 should be 10.23");
	}

	@Test
	@DisplayName("Price of security 120 should be 10.26")
	public void testPriceOfSecurity120_2() {
		pricingSource.subscribe(new BloombergPriceSourceListener("Listener1"));
		pricingSource.subscribe(new BloombergPriceSourceListener("Listener2"));
		pricingSource.referencePriceChanged(120, 10.28);
		pricingSource.referencePriceChanged(120, 10.29);
		pricingSource.referencePriceChanged(120, 10.26);
		assertEquals(10.26, pricingSource.get(120), "Price of security 120 should be 10.26");
	}

	@Test
	@DisplayName("Price of security 245 should be 100")
	public void testPriceOfSecurity245() {
		assertEquals(100, pricingSource.get(245), "Price of security 100 should be 100");
	}
}
