package com.example.marketmaker.testcase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.example.marketmaker.*;

public class OrderTest {
	@Test
	@DisplayName("Order object should be created properly")
	public void testCreateOrderObject() {
		Order order;
		try {
			order = new Order("123 BUY 100");
			assertEquals(Side.BUY, order.getSide(), "This is BUY Order");
			assertEquals(123, order.getSecurityId(), "Security Id is 123");
			assertEquals(100, order.getQuantity(), "Quantity is 100");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@DisplayName("Order object should be created properly")
	public void testCreateOrderObject2() {
		Order order;
		try {
			order = new Order("225 SELL 1520");
			assertEquals(Side.SELL, order.getSide(), "This is SELL Order");
			assertEquals(225, order.getSecurityId(), "Security Id is 225");
			assertEquals(1520, order.getQuantity(), "Quantity is 1520");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
