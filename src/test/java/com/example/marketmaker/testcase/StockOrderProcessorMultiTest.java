package com.example.marketmaker.testcase;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.example.marketmaker.*;

public class StockOrderProcessorMultiTest {
	private StockOrderProcessorMulti processor;
	private QuoteCalculationEngine quoteCalculationEngine;
	private ReferencePriceSource referencePriceSource;
	private String[] buyOrders;

	@BeforeEach
	void setUp() throws Exception {
		processor = new StockOrderProcessorMulti();
		quoteCalculationEngine = new StockQuoteCalculationEngine();
		referencePriceSource = new BloombergPriceSource();

		int size = 10;
		buyOrders = new String[size];
		for (int i = 0; i < size; i++) {
			buyOrders[i] = Integer.toString(i + 120) + " BUY 1200";
		}
	}

	@Test
	@DisplayName("calculateQuotesConccurent should be same as calculateQuotes")
	public void testCalculateQuotesConcurrentShouldBeSameAsCalculateQuotes() {
		try {
			var result = StockOrderProcessor.calculateQuotes(buyOrders, referencePriceSource, quoteCalculationEngine);
			var resultConcurrent = processor.calculateQuotesConccurent(buyOrders, referencePriceSource,
					quoteCalculationEngine);
			for (String order : buyOrders) {
				assertEquals(result.get(order), resultConcurrent.get(order), "Result should be identical");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
