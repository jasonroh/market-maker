package com.example.marketmaker.testcase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.example.marketmaker.*;

public class StockOrderProcessorTest {
	private QuoteCalculationEngine quoteCalculationEngine;
	private ReferencePriceSource referencePriceSource;

	@BeforeEach
	void setUp() throws Exception {
		quoteCalculationEngine = new StockQuoteCalculationEngine();
		referencePriceSource = new BloombergPriceSource();
	}

	@Test
	@DisplayName("Calculated Quote Price for Buy Side Order for Liquid Stock should be 100.02")
	public void testQuoteForOrderMessage_1() {
		try {
			double quote = StockOrderProcessor.calculateQuote("125 BUY 1200", referencePriceSource,
					quoteCalculationEngine);
			assertEquals(12.232446000000001, quote,
					"Calculated Quote Price for Buy Side Order for Liquid Stock should be 100.02");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@DisplayName("Calculated Quote Price for Sell Side Order for Liquid Stock should be 99.98")
	public void testQuoteForOrderMessage_2() {
		try {
			double quote = StockOrderProcessor.calculateQuote("125 SELL 1300", referencePriceSource,
					quoteCalculationEngine);
			assertEquals(12.227554, quote,
					"Calculated Quote Price for Sell Side Order for Liquid Stock should be 99.98");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@DisplayName("Calculated Quote Price for Buy Side Order for Liquid Stock should be 75.75834864000001")
	public void testQuoteForOrderMessage_3() {
		try {
			double quote = StockOrderProcessor.calculateQuote("123 BUY 100", referencePriceSource,
					quoteCalculationEngine);
			assertEquals(75.75834864000001, quote,
					"Calculated Quote Price for Buy Side Order for Liquid Stock should be 75.75834864000001");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	@DisplayName("Calculated Quote Price for Sell Side Order for Liquid Stock should be 75.72805136")
	public void testQuoteForOrderMessage_4() {
		try {
			double quote = StockOrderProcessor.calculateQuote("123 SELL 1000", referencePriceSource,
					quoteCalculationEngine);
			assertEquals(75.72805136, quote,
					"Calculated Quote Price for Sell Side Order for Liquid Stock should be 75.72805136");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
