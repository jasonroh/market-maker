package com.example.marketmaker.testcase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.example.marketmaker.*;

public class StockQuoteCalculationEngineTest {
	private QuoteCalculationEngine engine;

	@BeforeEach
	void setUp() throws Exception {
		engine = new StockQuoteCalculationEngine();
	}

	@Test
	@DisplayName("Calculated Quote Price for Buy Side Order for Liquid Stock should be 100.02")
	public void testBuyQuotePriceLiqud() {
		assertEquals(100.02, engine.calculateQuotePrice(123, 100, true, 120),
				"Calculated Quote Price for Buy Side Order for Liquid Stock should be 100.02");
	}

	@Test
	@DisplayName("Calculated Quote Price for Sell Side Order for Liquid Stock should be 99.98")
	public void testSellQuotePriceLiquid() {
		assertEquals(99.98, engine.calculateQuotePrice(123, 100, false, 120),
				"Calculated Quote Price for Sell Side Order for Liquid Stock should be 99.98");
	}

	@Test
	@DisplayName("Calculated Quote Price for Buy Side Block Order for Liquid Stock should be 100.015")
	public void testBlockBuyQuotePriceLiquid() {
		assertEquals(100.015, engine.calculateQuotePrice(123, 100, true, 12000),
				"Calculated Quote Price for Buy Side Block Order for Liquid Stock should be 100.015");
	}

	@Test
	@DisplayName("Calculated Quote Price for Sell Side Block Order for Liquid Stock should be 99.985")
	public void testBlockSellQuotePrice() {
		assertEquals(99.985, engine.calculateQuotePrice(123, 100, false, 12000),
				"Calculated Quote Price for Sell Side Block Order for Liquid Stock should be 99.985");
	}

	@Test
	@DisplayName("Calculated Quote Price for Buy Side Order for Iliquid Stock should be 100.03")
	public void testBuyQuotePriceIlliquid() {
		assertEquals(100.03, engine.calculateQuotePrice(223, 100, true, 220),
				"Calculated Quote Price for Buy Side Order for Iliquid Stock should be 100.03");
	}

	@Test
	@DisplayName("Calculated Quote Price for Sell Side Order for Iliquid Stock should be 99.97")
	public void testSellQuotePriceIlliquid() {
		assertEquals(99.97, engine.calculateQuotePrice(223, 100, false, 220),
				"Calculated Quote Price for Sell Side Order for Iliquid Stock should be 99.97");
	}

	@Test
	@DisplayName("Calculated Quote Price for Buy Side Block Order for Illiquid Stock should be 100.025")
	public void testBulkBuyQuotePrice() {
		assertEquals(100.025, engine.calculateQuotePrice(223, 100, true, 12000),
				"Calculated Quote Price for Buy Side Block Order for Illiquid Stock should be 100.025");
	}

	@Test
	@DisplayName("Calculated Quote Price for Sell Side Block Order for Illiquid Stock should be 99.975")
	public void testBulkSellQuotePrice() {
		assertEquals(99.975, engine.calculateQuotePrice(223, 100, false, 12000),
				"Calculated Quote Price for Sell Side Block Order for Illiquid Stock should be 99.975");
	}
}
