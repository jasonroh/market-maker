package com.example.marketmaker.testcase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.example.marketmaker.*;

public class StockSecurityIdValidatorTest {
	@Test
	@DisplayName("Security ID 123 is valid id")
	public void testValidId() {
		assertEquals(true, StockSecurityIdValidator.validateSecurityId(123), "Security ID 123 is valid id");
	}

	@Test
	@DisplayName("Security ID 223 is valid id")
	public void testValidId2() {
		assertEquals(true, StockSecurityIdValidator.validateSecurityId(223), "Security ID 223 is valid id");
	}

	@Test
	@DisplayName("Security ID 1231 is not valid id")
	public void testNotValidId() {
		assertEquals(false, StockSecurityIdValidator.validateSecurityId(1231), "Security ID 1231 is not valid id");
	}

	@Test
	@DisplayName("Security ID 323 is not valid id")
	public void testNotValidId2() {
		assertEquals(false, StockSecurityIdValidator.validateSecurityId(323), "Security ID 323 is not valid id");
	}
}
